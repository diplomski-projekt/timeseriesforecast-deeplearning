# TimeSeriesForecast-DeepLearning

Predviđanje vremenskih serija korištenjem dubokih modela.

## Podaci
| Direktorij | Opis |
| ------ | ------ |
| Data/Kupa/Flow | csv datoteke koje sadrže podatke o protoku |
| Data/Kupa/Rainfall | csv datoteke koje sadrže podatke o padalinama |
| Data/Kupa/WaterLevel | csv datoteke koje sadrže podatke o vodostaju rijeke * |

\* može sadržavati negativne vrijednosti vodostaja rijeke (objašnjenje: http://hidro.dhz.hr/hidroweb/pocetna/Vodostaj/Vodostaj.html).

### Flow
| Ime kolone | Opis |
| ------ | ------ |
| Datum | datum mjerenja (dd.mm.yyyy) |
| Protok | protok (m³/s) | 

### Rainfall
| Ime kolone | Opis |
| ------ | ------ |
| Datum | datum mjerenja (yyyy-mm-dd) |
| Padaline | količina padalina (mm) | 

### WaterLevel
| Ime kolone | Opis |
| ------ | ------ |
| Datum | datum mjerenja (dd.mm.yyyy) |
| Vodostaj | vodostaj (cm) | 