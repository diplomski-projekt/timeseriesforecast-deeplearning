import numpy as np
import pandas as pd
import datetime as dt

from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error

import matplotlib.pyplot as plt

def baseline_predictor(train, test):
    """Persistence forecast. 
    The observation from the prior time step (t-1) is used to predict the observation at the current time step (t):
    predicted[t] = x[t-1]
    
    Parameters
    ----------
    train: 1D numpy.ndarray
        Contains time series values used for training
    test: 1D numpy.ndarray
        Contains time series values used for testing
        
    Returns
    -------
    (numpy.ndarray, Float, Float)
        Predictions, RMSE calculated on test set and MAE calculated on test set
    """
    history = [x for x in train]
    predictions = []
    for i in range(len(test)):
        # make prediction
        predictions.append(history[-1])
        # observation
        history.append(test[i])
    
    rmse = np.sqrt(mean_squared_error(y_true=test, y_pred=predictions))
    mae = mean_absolute_error(y_true=test, y_pred=predictions)
    return predictions, rmse, mae    

def adj_r2_score(r2, n, k):
    return 1 - ((1 - r2) * ((n - 1) / (n - k - 1)))


def load_data_from_csv(path, datetime_format='%d.%m.%Y', date_column_name='Datum', separator=';'):
    """
    Loads data from csv file to pandas dataframe.

    :param path: string, path to the CSV file which contains time series data
    :param datetime_format: string, format of datetime column in the CSV file
    :param date_column_name: string, name of the datetime column in the CSV file
    :param separator: string, CSV column separator
    :return: DataFrame, DataFrame which contains data from CSV file
    """
    df = pd.read_csv(path, sep=separator)
    # convert string date to date type
    df['Datum'] = [dt.datetime.strptime(date_str, datetime_format) for date_str in df[date_column_name]]
    return df


def add_monht_features(df):
    '''
    Adds one-hot month features.
    Example
        If month is October then new features will be [0,0,0,0,0,0,0,0,0,1,0,0].
    :param df: dataframe which contains datetime column named 'Datum'
    :return: (DataFrame, DataFrame). First dataframe is passed dataframe with 12 new columns, one for each month (values in this columns are binary and should
             not be scaled). Second dataframe is newly created and contains one-hot encoded months features.
    '''
    df['Mjesec'] = df.Datum.dt.month
    months = pd.get_dummies(df.Mjesec, prefix='Mjesec')
    df = pd.concat([df, months], axis=1)
    df = df.drop(columns=['Mjesec'])
    return df, months


def transform_ts_to_supervised(values, lag=1):
    """
    Generates supervised dataset from the given time series values.
    :param values: 1D numpy.ndarray or DataFrame, time series values
    :param lag: int, defines lag between labels and values
    :return: DataFrame, DataFrame which conatins examples from given values and labels.
             Columns are named 'X' (for examples) and 'y' (for labels)
    """
    in_df = pd.DataFrame(values)

    out_df = in_df.shift(periods=1).fillna(0)
    for l in range(2, lag + 1):
        shifted_df = in_df.shift(periods=l).fillna(0)
        out_df = pd.concat([out_df, shifted_df], axis=1)

    out_df = pd.concat([in_df, out_df], axis=1)

    # generate new column names
    num_features = in_df.shape[1]
    new_names = np.array([[str(out_df.columns[j]) + '(t-' + str(i) + ')' for j in range(num_features)] for i in
                          range(0, lag + 1)]).flatten()
    out_df.columns = new_names
    return out_df


def scale(train_values, test_values, feature_range=(-1, 1)):
    """
    Scales values. Each value will be scaled to given range.
    :param train_values: numpy.ndarray, values used for scaler fitting, after fitting, values are scaled
    :param test_values: numpy.ndarray, values that will be scaled
    :param feature_range: tuple, lower and upper bound for scaled values
    :return: (numpy.ndarray, numpy.ndarray, scaler), scaled train values, scaled test values, scaler object (can be used for inverse scaling)
    """
    scaler = MinMaxScaler(feature_range=feature_range)
    scaler = scaler.fit(train_values)

    scaled_train_values = scaler.transform(train_values)
    scaled_test_values = scaler.transform(test_values)

    return scaled_train_values, scaled_test_values, scaler


def plot_predictions_and_labels(labels, predictions, step=30, save_pictures=False, save_directory=None,
                                picture_name=None):
    """
    Plots or saves label values and predicted values on the same figure.
    :param labels: true values
    :param predictions: predicted values
    :param step: defines number of time steps that will be plotted on one figure
    :param save_pictures: flag, if True, figures will be saved
    :param save_directory: location where figures will be saved
    :param picture_name: root name, extension will be concatenated
    :return: None
    """

    labels_size = labels.shape[0]
    if step == None:
        _tuple = zip(list(range(0, 1)), list(range(labels_size-1, labels_size)))
        plt.figure(num=None, figsize=(26, 6), dpi=80, facecolor='w', edgecolor='k')
    else:
        _tuple = zip(list(range(0, labels_size - step, step)), list(range(step, labels_size, step)))

    counter = 1
    for start, stop in _tuple:
        plt.plot(labels[start:stop], label='True')
        plt.plot(predictions[start:stop], label='LSTM')
        plt.xlabel('Time')
        plt.ylabel('Measurements')
        plt.legend()
        if save_pictures:
            plt.savefig(save_directory + picture_name + '_' + str(counter) + '.png')
            #plt.gcf().clear()
            plt.cla()
            counter += 1
        else:
            plt.show()

    plt.close('all')
